module MyVars
  OS       = "puppetlabs/centos-7.2-64-puppet"
  OS_URL   = "https://atlas.hashicorp.com/puppetlabs/boxes/centos-7.2-64-puppet"
  PUPPET   = "scripts/noop.sh"
  RPM_URL  = "http://yum.postgresql.org/9.2/redhat/rhel-7-x86_64/pgdg-centos92-9.2-2.noarch.rpm"
  RPM_NAME = "pgdg-centos92-9.2-2.noarch"
end
